import * as React from 'react';

import {MainContainer} from './container/MainContainer';

import './App.css';



class App extends React.Component {
  public render() {
    return (
      <MainContainer />
    );
  }
}

export default App;
