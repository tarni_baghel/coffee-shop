import * as React from 'react';
import {Button, Form, FormControl, Tab, Tabs} from 'react-bootstrap';
import {FaMapMarkerAlt} from 'react-icons/fa';

import './TabContainer.css';

export const TabContainer = () => {
    return (
        <div className='tabContainer'>
            <Tabs defaultActiveKey="webboard" id="uncontrolled-tab-example" className='tabbar'>
                <Tab eventKey="order" title="ORDER" className='tab'/>
                <Tab eventKey="member" title="MEMBER" className='tab'/>
                <Tab eventKey="webboard" title="WEBBOARD" className='tab' style={{'color': '#964b00'}}>
                <p > <FaMapMarkerAlt /> Lets go for a ? </p>
                <p > <FaMapMarkerAlt /> Lets go for a ? </p>
                <article>Lets go for a ?</article>
                <Form inline={true} style={{'display': 'block'}}>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                </Form>
                </Tab>
            </Tabs>
        </div>
    )
}