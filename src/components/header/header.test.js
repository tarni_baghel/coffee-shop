import React from 'react';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import {configure, shallow} from 'enzyme';
import {findByTestAttr} from './../../../Utils';
import Header from './index';

configure({adapter: new EnzymeAdapter()});

const setUp = (props={})=>{
    const component =  shallow(<Header {...props}/>)
    return component;
}

describe("Header Component",()=>{
    let component;
    beforeEach(()=>{
        component=setUp();
    })

    test("should render without error",()=>{
        const wrap = findByTestAttr(component,'.header')
        expect(wrap.length).toBe(1);
    });
})