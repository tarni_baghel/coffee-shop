import * as React from 'react';
import './LinkBox.css';

export const LinkBox = () => {
    return (
        <div className='linkBox'>
            <a href="https://www.w3schools.com">
                <img alt="W3Schools" src="https://image.flaticon.com/icons/svg/46/46010.svg" style={{'width':"100px", 'height':"100px", 'marginTop':'50px'}}/>
            </a>
        </div>
    )
}   