import React from 'react';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import {configure, shallow} from 'enzyme';
import {findByTestAttr} from './../../Utils';
import {LinkBox} from './LinkBox';

configure({adapter: new EnzymeAdapter()});

const setUp = (props={})=>{
    const component =  shallow(<LinkBox {...props}/>)
    return component;
}

describe("LinkBox Component",()=>{
    let component;
    beforeEach(()=>{
        component=setUp();
    })

    test("should render without error",()=>{
        const wrap = findByTestAttr(component,'.linkBox')
        expect(wrap.length).toBe(1);
    });
})