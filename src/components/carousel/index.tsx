import * as React from 'react';
import './carousel.css';
class Carousel extends React.Component{
    public render() {
        return (
          <div>
   <div id="demo" className="carousel slide" data-ride="carousel">

<ul className="carousel-indicators">
  <li data-target="#demo" data-slide-to="0" className="active"/>
  <li data-target="#demo" data-slide-to="1"/>
  <li data-target="#demo" data-slide-to="2"/>
</ul>

<div className="carousel-inner">
  <div className="carousel-item active">
    <img src="https://blog.send-anywhere.com/wp-content/uploads/2016/02/pictures-cup-of-coffee-hd-wallpapers.jpg"/>
  </div>
  <div className="carousel-item">
    <img src="https://slidervilla.com/dbox/files/2014/05/4.jpg"/>
  </div>
  <div className="carousel-item">
    <img src="https://scrambledjakes.com/wp-content/uploads/2017/07/MainSlider_6.jpg"/>
  </div>
</div>

<a className="carousel-control-prev" href="#demo" data-slide="prev">
  <span className="carousel-control-prev-icon"/>
</a>
<a className="carousel-control-next" href="#demo" data-slide="next">
  <span className="carousel-control-next-icon"/>
</a>
</div> </div>
        );
      }
}

export default Carousel;