import React from 'react';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import {configure, shallow} from 'enzyme';
import {findByTestAttr} from './../../../Utils';
import Carousel from './index';

configure({adapter: new EnzymeAdapter()});

const setUp = (props={})=>{
    const component =  shallow(<Carousel {...props}/>)
    return component;
}

describe("Carousel Component",()=>{
    let component;
    beforeEach(()=>{
        component=setUp();
    })

    test("should render without error",()=>{
        const wrap = findByTestAttr(component,'.carousel-item')
        expect(wrap.length).toBe(3);
    });
})