import React from 'react';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import {configure, shallow} from 'enzyme';
import {findByTestAttr} from './../../Utils';
import {TabContainer} from './TabContainer';

configure({adapter: new EnzymeAdapter()});

const setUp = (props={})=>{
    const component =  shallow(<TabContainer {...props}/>)
    return component;
}

describe("Tab Component",()=>{
    let component;
    beforeEach(()=>{
        component=setUp();
    })

    test("should render without error",()=>{
        const wrap = findByTestAttr(component,'.tabContainer')
        expect(wrap.length).toBe(1);
    });
})