import * as React from 'react';
import './banner.css';

class Banner extends React.Component {
  public render() {
    return (
      <div className="image">
          <img src="https://vinylbannersprinting.co.uk/wp-content/uploads/2016/05/RB05-AA-demo.png"/>
          </div>
    );
  }
}

export default Banner;
