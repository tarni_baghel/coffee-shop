import React from 'react';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import {configure, shallow} from 'enzyme';
import {findByTestAttr} from './../../../Utils';
import Banner from './index';

configure({adapter: new EnzymeAdapter()});

const setUp = (props={})=>{
    const component =  shallow(<Banner {...props}/>)
    return component;
}

describe("Banner Component",()=>{
    let component;
    beforeEach(()=>{
        component=setUp();
    })

    test("should render without error",()=>{
        const wrap = findByTestAttr(component,'.image')
        expect(wrap.length).toBe(1);
    });
})