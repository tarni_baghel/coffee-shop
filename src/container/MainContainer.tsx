import * as React from 'react';

import {LinkContainer} from './LinkContainer';

import {ProductGrid} from '../components/ProductGrid';

import Header from '../components/header';

import Carousel from '../components/carousel';

import Banner from '../components/Banner';
import './MainContainer.css';

export const MainContainer = () => {
    return (
        <div className='mainContainer'>
            <Header />
            <Carousel/>
            <ProductGrid />
            <LinkContainer />
            <Banner />
            
        </div>
    )
}