import * as React from 'react';

import {LinkBox} from '../components/LinkBox';
import {TabContainer} from '../components/TabContainer';

import './LinkContainer.css';

export const LinkContainer = () => {
    return (
        <div className='linkContainer'>
            <LinkBox />
            <TabContainer />
        </div>
    )
}